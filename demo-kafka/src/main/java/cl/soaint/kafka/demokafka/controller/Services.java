package cl.soaint.kafka.demokafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.soaint.kafka.demokafka.utils.ProducerMessage;


@RestController
public class Services {

	@Autowired
	ProducerMessage producer;


	@PostMapping("/sendMessageTopic")
	public void greeting(@RequestParam("message") String message) {
		producer.sendMessage(message);
	}
}
